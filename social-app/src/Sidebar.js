
import React from 'react';
import "./Sidebar.css";
import SidebarRow from "./SidebarRow";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EmojiFlagsIcon from '@material-ui/icons/EmojiFlags';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import PeopleIcon from '@material-ui/icons/People';
import ChatIcon from '@material-ui/icons/Chat';
import StorefrontIcon from '@material-ui/icons/Storefront';
import VideoLibraryIcon from '@material-ui/icons/VideoLibrary';



function Sidebar() {
    return (
        <div className="sidebar"> 
        <SidebarRow src="https://form-physic.com/wp-content/uploads/2019/03/avatar-1.jpg" title="Eldin" />
        <SidebarRow 
        Icon={LocalHospitalIcon}
        title='COVID-INFO  '/>

        <SidebarRow Icon={EmojiFlagsIcon} title='Pages  '/>
        <SidebarRow Icon={PeopleIcon} title='Friends  '/>
        <SidebarRow Icon={ChatIcon} title='Messenger  '/>
        <SidebarRow Icon={StorefrontIcon} title='Marketplace  '/>
        <SidebarRow Icon={VideoLibraryIcon} title='Videos  '/>
        <SidebarRow Icon={ExpandMoreIcon} title='Marketplace '/>
       
            
        </div>
    )
}

export default Sidebar
