import React from "react";
import { BrowserRouter as Router, Route } from
"react-router-dom";
import { HomePage, BlogPage, ContactPage, NoPage }
from "./pages";
import Navbar from "./navbar";
export default () => {
return (
 <Router>
 <Navbar />
 <Route path="/home" exact component={HomePage} />
 <Route path="/produits" component={BlogPage} />
 <Route path="/contact" component={ContactPage} />
 <Route component={NoPage} />
 </Router>
 );
};